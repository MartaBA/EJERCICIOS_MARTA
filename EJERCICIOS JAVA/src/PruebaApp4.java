
public class PruebaApp4 {
	
	static int variableGlobal=10;
	
	public static void metodoPrueba () {
		
		int variableMetodo=40;
		for (int i=0;i<10;i++) {
			System.out.println("La variable i vale "+i); //aqu� si existe la variable i
		}
		//System.out.println("La variable i vale "+i); //aqui ya no existe la variable i
	}
	
	public static void main(String[] args) {
		
		//System.out.println("La variable local vale "+variableMetodo); //aqui ya no existe la variable de metodo
		
		int variableLocal1=20;
		if (variableGlobal==10){
			int variableLocal2=30;
			System.out.println("La variable local 1 vale "+variableLocal1); //Aqui si existe, ya que se ha declarado en un bloque c�digo superio	
		}
		//System.out.println("La variable local 2 vale "+variableLocal2); //Aqui ya no existe la variable 2
		System.out.println("La variable local Global vale "+variableGlobal); //aqui sigue existiendo
		
		metodoPrueba();
	}

}
